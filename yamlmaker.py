#!/usr/bin/env python3
'''
Generates a yaml-file for Spades
'''
import sys
import os


class yamlmaker:

    def __init__(self, filepath, filename):
        self.filepath = filepath
        self.yamfile = filename
        self.fastalist = []
        self.count = 0

    def fastagetter(self):
        # gets all files and combines the needed files in a list
        # a mac problem
        filelist = os.listdir(self.filepath)
        for filename in filelist:
            if filename.startswith(".DS_Store"):
                pass
            else:
                self.fastalist.append(self.filepath + filename)
        return self.fastalist

    def yamlthing(self):
        try:
            yamfile = open(self.yamfile, "x")
        except FileExistsError:
            print("file already exist, to prevent loss of data, please store existing yamlfile somewhere else "
                     "and try again.")
            yamfile = open(self.yamfile, "w")
        # writes the first lines of the yamlfile
        yamfile.write("[\n    {\n      type: \"nanopore\",\n      single reads: [\n")
        for filename in self.fastalist:
            # places the fastafilenames in the yamlfile.
            # the count is needed to finnish the yamlfile.
            self.count += 1
            yamfile.write("       \"{}\"".format(filename))
            if self.count == len(self.fastalist):
                # all fastqs are in the file, it can be closed propperly.
                yamfile.write("\n      ]")
            else:
                # not all of the fastqs are in the file yet, it needs to run longer.
                yamfile.write(",\n")
        # finishes the file
        yamfile.write("\n    }\n]")
        yamfile.close()
        return yamfile


def main(argv=None):
    if argv is None:
        argv = sys.argv
    yamlfile = yamlmaker(argv[1], argv[2])
    yamlfile.fastagetter()
    yamlfile.yamlthing()
    return 0


if __name__ == "__main__":
    sys.exit(main())

