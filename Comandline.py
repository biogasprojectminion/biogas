#!/usr/bin/env python
"""
The back end for the GUI anD Comandline version
of the pipeline
"""
# import statements
import sys
import time
import os
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from subprocess import call
from os import listdir
from os.path import isfile, join


class mainHandler:
    def __init__(self):
        self.path = None
        self.realtime = None
        self.outpath = None
        self.index = None
        self.mapper = None
        return

    def pipeline_check(self):
        '''checks what kind of pipeline the user wants'''
        if self.realtime == "True":
            # The given directory is monitored for new files
            self.monitor()
        else:
            # Realtime is false so a batchHandler is created
            pipeline_batch = Pipeline(self.path, self.outpath, self.index)
            # All the fast5 files in the given directory are converted to fastq
            pipeline_batch.create_fastq("Reads")
            #pipeline_batch.quality_control("Reads.fastq")
            if self.mapper == "Centrifuge":
                pipeline_batch.centrifuge_mapper("Reads")
            else:
                pipeline_batch.bowtie_mapper()
        return

    def monitor(self):
        '''Monitors a given directory'''
        event_handler = RealTimeEventHandler(self.path, self.outpath, self.index, self.mapper)
        observer = Observer()
        observer.schedule(event_handler, path=self.path, recursive=False)
        observer.start()

        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            observer.stop()
        observer.join()
        return

    def set_real_time(self, realtime):
        '''sets the atribute realtime'''
        # Checks if realtime is valid
        if realtime != "True" and realtime != "False":
            return "Error: Realtime is not True or False"

        self.realtime = realtime
        return

    def setoutpath(self, outpath):
        '''sets the atribute outpath'''
        # checks if the path is valid
        if os.path.isdir(outpath):
            pass
        else:
            return "Error: outpath is not a valid path"

        self.outpath = outpath
        return

    def setpath(self, path):
        '''sets the atribute path'''
        # checks if the path is valid
        if os.path.isdir(path):
            pass
        else:
            return "Error: path is not a valid path"

        self.path = path
        return

    def set_index(self, index):
        '''sets the atribute centrifuge_index'''
        # checks if the centrifuge index does exists
        # print(index)
        # if os.path.isfile(index):
        #     pass
        # else:
        #     return "Error: Index doesn't exists"

        self.index = index
        return

    def set_mapper(self, mapper):
        '''sets the atribute bowtie_index'''
        # checks if the bowtie index does exists
        if mapper != "Bowtie" and mapper != "Centrifuge":
            return "Error: mapper is not Bowtie or Centrifuge"

        self.mapper = mapper
        return


class Pipeline:
    def __init__(self, path, outpath, index):
        self.path = path
        self.outpath = outpath
        self.index = index
        return

    def create_fastq(self, filename):
        '''Creates from fast5 files fastq files in a given directory and writes them to a given directory'''
        file = open("{}{}.fastq".format(self.outpath, filename), "w")
        call(["poretools", "fastq", "{}".format(self.path)], stdout=file)
        return

    def quality_control(self, inputfile):
        '''Controls the data and gives a outputfile with the quality of the data'''
        output = "{}/Quality_Control_of_{}".format(self.outpath, inputfile)
        # checks if the output directory already exists if not the directory is created
        if os.path.isdir(output):
            pass
        else:
            os.mkdir(output)
        call(["fastqc", "{}/{}".format(self.outpath, inputfile), "-o", output])
        return

    def centrifuge_mapper(self, output_file):
        call(["centrifuge-master/centrifuge", "-x", "{}".format(self.index), "-S", "{}.txt".format(output_file), "-U",
              "{}/Reads.fastq".format(self.outpath)])
        return

    def bowtie_mapper(self):
        call(["centrifuge-master/centrifuge", "-x", "{}".format(self.index), "-S", "bowtie_output.sam", "-U",
              "{}/Reads.fastq".format(self.outpath)])
        return


class RealTimeEventHandler(FileSystemEventHandler, Pipeline):
    '''If the directory is altered this class greates a fastq file for every new fast5 file'''
    def __init__(self, path, outpath, index, mapper, ):
        super(RealTimeEventHandler, self).__init__(path, outpath, index)
        self.passed_files = []
        self.mapper = mapper
        return

    def on_created(self, event):
        '''If a new file is created in the directory this function prints the event and
        creates of the new fast5 file a fastq file'''
        # prints the event that has occurred
        print(event)
        # Gets all the files of a given directory
        onlyfiles = [f for f in listdir(self.path) if isfile(join(self.path, f))]
        for file in onlyfiles:
            # checks if the file is already converted
            if file in self.passed_files:
                pass
            elif not file.startswith("."):
                self.create_fastq("Read{}".format(len(self.passed_files)+1))
                # files already converted are appended to the list passed files
                self.passed_files.append(file)
        if self.mapper == "Centrifuge":
            self.merge_to_one()
            self.centrifuge_mapper()
        else:
            self.merge_to_one()
            self.bowtie_mapper()
        return

    def merge_to_one(self):
        '''Merges multiple fastq files to one'''
        outputfilename = '{}/Reads.fastq'.format(self.outpath)
        outputfile = open(outputfilename, 'w')
        for filename in os.listdir(self.outpath):
            if os.path.getsize(outputfilename) > 0:
                pass
            elif not filename.startswith("."):
                opened = open('{}/{}'.format(self.outpath, filename), 'r')
                for line in opened:
                    outputfile.write(line)
            if filename == "Reads.fastq":
                pass
            else:
                os.remove("{}/{}".format(self.outpath, filename))
        outputfile.close()
        return


def main():
    myhandler = mainHandler()
    errorlist = list()
    errorlist.append(myhandler.setpath(sys.argv[1]))
    errorlist.append(myhandler.set_real_time(sys.argv[2]))
    errorlist.append(myhandler.setoutpath(sys.argv[3]))
    errorlist.append(myhandler.set_index(sys.argv[4]))
    errorlist.append(myhandler.set_mapper(sys.argv[5]))
    for error in errorlist:
        if error:
            print(error)
            return 1
    myhandler.pipeline_check()
    return 0

if __name__ == '__main__':
    sys.exit(main())
