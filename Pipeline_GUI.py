#!/usr/bin/python 3
"""
GUI build for Biogas
"""

import argparse
from gooey import Gooey, GooeyParser
import Comandline


@Gooey()
class Gui:
    def __init__(self):
        self.args = ""

    def parsing(self):
        parser = GooeyParser(description="processes raw Minion data into Fastq data and maps it with Centrifuge or Bowtie2")

        # makes a field where you can fill in the path for the output
        parser.add_argument(
            'Path',
            metavar="What is the inputpath",
            action="store"
        )

        parser.add_argument(
            "outPath",
            metavar="where to place the output",
            action="store"
        )
        parser.add_argument(
            "index",
            metavar="index",
            action="store",
            help="PathToIndex/indexname(.rev.bt2)"
        )
        # makes a checkbox if you want real time reads
        parser.add_argument(
            "-r", "--realTimeCheck",
            metavar='Do you want to have real time reads?',
            action='store_true',
            help='Turn this on for real time reads')
        parser.add_argument(
            "mapper",
            action="store",
            choices=["Centrifuge", "Bowtie"],
            help="Choose your mapping tool"
        )
        self.args = parser.parse_args()
        path = self.args.Path
        realtime = self.args.realTimeCheck
        outpath = self.args.outPath
        index = self.args.index
        mapper = self.args.mapper
        if realtime:
            realtime = "True"
        else:
            realtime = "False"

        return path, realtime, outpath, index, mapper



def main():
    gui = Gui()
    path, realtime, outpath, index, mapper = gui.parsing()
    handler = Comandline.mainHandler()
    # for error handling
    errorlist = list()
    errorlist.append(handler.setpath(path))
    errorlist.append(handler.set_real_time(realtime))
    errorlist.append(handler.setoutpath(outpath))
    errorlist.append(handler.set_index(index))
    errorlist.append(handler.set_mapper(mapper))
    for error in errorlist:
        if error:
            print(error)
            return 1
    handler.pipeline_check()
    return 0


if __name__ == '__main__':
    main()
